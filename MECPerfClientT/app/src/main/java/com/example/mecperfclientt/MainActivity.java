package com.example.mecperfclientt;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Map;
import measure.Measure;
import measurements.Measurements;

public class MainActivity extends AppCompatActivity {
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int CMDPORT = 6792;//6789
    private static final int TCPPORT = 6791;//6788
    private static final int UDPPORT = 6790;//6787
    private static final String SERVERIP = "131.114.73.2";
    private static final String AGGREGATORIP = "131.114.73.3";
    private static final int AGGRPORT = 6766;
    private static final Measurements operator = new Measurements();
    private static final String TESTFILEPATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/input.txt";
    private static final int PKTSIZE = 1024;

    private AsyncCMD mAsyncTask = null;
    private Integer id = 0;

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("STATE", "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner function_spinner = (Spinner) findViewById(R.id.Function_spinner);
        CharSequence[] fun = getResources().getStringArray((R.array.function_array));
        ArrayAdapter<CharSequence> adfun = new ArrayAdapter<CharSequence>( this, android.R.layout.simple_spinner_item, fun);
        adfun.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        function_spinner.setAdapter(adfun);

        verifyStoragePermissions(this);

        Button TCPButton = (Button) findViewById(R.id.TCPBandwidthbutton);
        TCPButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAsyncTask = new AsyncCMD("TCPBUTTON");
                mAsyncTask.execute((Void) null);
                Log.d("TASK","Task Eseguito");
            }
        });

        Button UDPButton = (Button) findViewById(R.id.UDPBandwidthbutton);
        UDPButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAsyncTask = new AsyncCMD("UDPBUTTON");
                mAsyncTask.execute((Void) null);
                Log.d("TASK","Task Eseguito");
            }
        });

        Button TCPRTTButton = (Button) findViewById(R.id.TCPRTTbutton);
        TCPRTTButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAsyncTask = new AsyncCMD("TCPRTTBUTTON");
                mAsyncTask.execute((Void) null);
            }
        });

        Button UDPRTTButton = (Button) findViewById(R.id.UDPRTTbutton);
        UDPRTTButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAsyncTask = new AsyncCMD("UDPRTTBUTTON");
                mAsyncTask.execute((Void) null);
            }
        });
    }
    private class Result {
        private double value;
        private String type;

        public Result(double v, String t){
            value = v;
            type = t;
        }
    }

    protected static void sendDataToAggregator(String type, int id, String sender, String receiver, double latency, Map<Long, Integer> bandwidth){
        Log.d("STATE", "sendDataToAggregator()");
        Socket socket = null;
        ObjectOutputStream objOutputStream = null;
        try {
            socket = new Socket(InetAddress.getByName(AGGREGATORIP), AGGRPORT);
            objOutputStream = new ObjectOutputStream(socket.getOutputStream());
            Measure measure = new Measure();
            measure.setType(type);
            measure.setID(id);
            measure.setSender(sender);
            measure.setReceiver(receiver);
            measure.setLatency(latency);
            measure.setBandwidth(bandwidth);

            // write the message we want to send
            objOutputStream.writeObject(measure);
            Log.d("STATE", "sendDataToAggregator(): data sent");
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                objOutputStream.close(); // close the output stream when we're done.
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class AsyncCMD extends AsyncTask<Void, Void, Result>{

        private String cmd;
        public AsyncCMD(String cmd){
            super();
            this.cmd = cmd;
        }

        @Override
        protected Result doInBackground(Void... voids) {

            //Knowing button mode
            Spinner fun_spinner = (Spinner) findViewById(R.id.Function_spinner);
            String function = fun_spinner.getItemAtPosition(fun_spinner.getSelectedItemPosition()).toString();
            switch (cmd){
                case "TCPBUTTON" :
                    if(function.equals("Sender")){
                        sendCMD("TCPBandwidthSender" + " " + id.toString());
                        id++;

                        try {
                            operator.TCPBandwidthSender(new Socket(InetAddress.getByName(SERVERIP), TCPPORT), new File(TESTFILEPATH));
                            File file = new File(TESTFILEPATH);
                            if (file.exists())
                                Log.d("STATO", "exist");
                            else
                                Log.d("STATO", "doesn't exist");

                        }
                        catch (IOException e) {
                            Log.d("ERRORE", e.toString());
                        }
                        Result result = new Result(0.0, " Finished!");
                        return result;
                    }
                    else{
                        sendCMD("TCPBandwidthReceiver"+ " " + id.toString());
                        int idToAggregator = id;
                        id++;

                        Map<Long, Integer>  longIntegerMap = null;
                        try {
                            Socket tcpReceiver = new Socket(InetAddress.getByName(SERVERIP), TCPPORT);
                            longIntegerMap = operator.TCPBandwidthReceiver(tcpReceiver);

                        }
                        catch (IOException e) {
                            Log.d("ERRORE", e.toString());
                        }
                        //send data to Aggregator
                        sendDataToAggregator("TCPBandwidth", idToAggregator, "Observer", "Client", -1, longIntegerMap);
                        return  new Result(0.0, "Finished!");
                    }
                case "UDPBUTTON" :
                    if(function.equals("Sender")){
                        sendCMD("UDPLatencyPPSender"+ " " + id.toString());
                        id++;

                        try {
                            DatagramSocket udpsocket = new DatagramSocket();
                            udpsocket.connect(InetAddress.getByName(SERVERIP),UDPPORT);
                            operator.UDPLatencyPPSender(udpsocket, PKTSIZE, new File(TESTFILEPATH));
                        } catch (SocketException | UnknownHostException e) {
                            e.printStackTrace();
                        }

                        return  new Result(0.0, " Finished!");
                    }
                    else{
                        sendCMD("UDPLatencyPPReceiver"+ " " + id.toString());
                        int idToAggregator = id;
                        id++;

                        DatagramSocket udpsocket = null;
                        try {
                            udpsocket = new DatagramSocket();
                            byte[] buf;
                            String outString = "UDPLatencyPPReceiver";
                            buf = outString.getBytes();
                            DatagramPacket out = new DatagramPacket(buf, buf.length, InetAddress.getByName(SERVERIP), UDPPORT);
                            //Client has to send a packet to server to let the server knows Client's IP and Port
                            udpsocket.send(out);

                        } catch (IOException e) {
                            Log.d("ERROR", e.toString());
                        }

                        double latency = operator.UDPLatencyPPReceiver(udpsocket,PKTSIZE);
                        //send data to Aggregator
                        sendDataToAggregator("UDP", idToAggregator, "Observer", "Client", latency , null);
                        return new Result(latency, " Mbps");
                    }
                case "TCPRTTBUTTON" :
                    if(function.equals("Sender")){
                        sendCMD("TCPRTTC"+ " " + id.toString());
                        int idToAggregator = id;
                        id++;

                        double latency = 0.0;
                        try {
                            latency = operator.TCPRTTSender(new Socket(InetAddress.getByName(SERVERIP), TCPPORT));
                        } catch (IOException e) {
                            Log.d("ERRORE", e.toString());
                        }
                        //send data to Aggregator
                        sendDataToAggregator("TCPRTT", idToAggregator, "Client", "Observer", latency , null);

                        return new Result(latency,"Ms");
                    }
                    else{
                        sendCMD("TCPRTTMO"+ " " + id.toString());
                        id++;

                        try {
                            Socket tcpReceiver = new Socket(InetAddress.getByName(SERVERIP), TCPPORT);
                            operator.TCPRTTReceiver(tcpReceiver);
                        } catch (IOException e) {
                            Log.d("ERRORE", e.toString());
                        }
                        return new Result(0.0,"Server TCP Latency Test");
                    }
                case "UDPRTTBUTTON" :
                    if(function.equals("Sender")){
                        sendCMD("UDPRTTC"+ " " + id.toString());
                        int idToAggregator = id;
                        id++;

                        double latency = 0.0;
                        try {
                            DatagramSocket udpsocket = new DatagramSocket();
                            udpsocket.connect(InetAddress.getByName(SERVERIP),UDPPORT);
                            latency = operator.UDPRTTSender(udpsocket);
                        } catch (IOException e) {
                            Log.d("ERRORE", e.toString());
                        }
                        //send data to Aggregator
                        sendDataToAggregator("UDP", idToAggregator, "Client", "Observer", latency , null);
                        return new Result(latency,"Ms");
                    }
                    else{
                        sendCMD("UDPRTTMO" + " " + id.toString());
                        id++;

                        try {
                            DatagramSocket udpsocket = new DatagramSocket();
                            byte[] buf;
                            String outString = "UDPLatencyPPReceiver";
                            buf = outString.getBytes();
                            DatagramPacket out = new DatagramPacket(buf, buf.length, InetAddress.getByName(SERVERIP), UDPPORT);
                            //Client has to send a packet to server to let the server knows Client's IP and Port
                            udpsocket.send(out);
                            operator.UDPRTTReceiver(udpsocket);
                        } catch (IOException e) {
                            Log.d("ERRORE", e.toString());
                        }

                        return new Result(0.0,"Server UDP Latency Test");
                    }
            }
            return new Result(0.0,"");
        }

        @Override
        protected void onPostExecute(Result result){
            TextView resultTextView = (TextView) findViewById(R.id.resultTextView);
            resultTextView.setText( result.value + " " + result.type);
        }
        @Override
        protected void onCancelled() {
            mAsyncTask = null;
        }

        protected void sendCMD(String command){
            Socket socket = null;
            try {
                socket = new Socket(InetAddress.getByName(SERVERIP), CMDPORT);
            }
            catch (IOException e) {
                Log.d("ERRORE", e.toString());
            }

            // get the output stream from the socket.
            OutputStream outputStream = null;
            try {
                outputStream = socket.getOutputStream();
            } catch (IOException e) {
                Log.d("ERRORE", e.toString());
            }

            // create a data output stream from the output stream so we can send data through it
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
            try{
                // write the message we want to send
                dataOutputStream.writeUTF(command);
                dataOutputStream.flush(); // send the message
                dataOutputStream.close(); // close the output stream when we're done.
            }catch (IOException e){
                Log.d("ERRORE", e.toString());
            }
        }
    }
}
