package it.unipi.dii.common;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.Serializable;
import java.util.Map;

/**
 * This class is meant to be the packet sent to the aggregator in order to store measurement
 * coming from the three different entities of the system (Client, Observer, Remote Server)
 */
public class Measure implements Serializable {

    private String type;
    private int ID;
    private String sender;
    private String receiver;
    private Map<Long, Integer> bandwidth;
    private Double latency;
    private static final long serialVersionUID = 3919700812200232178L;



    public Measure(){
    }

    /**
     * Constructor
     * @param type TCP or UDP type of used protocol
     * @param ID  Unique identifier of the test
     * @param sender Name of the component that send the message
     * @param receiver Name of the component that receive the message
     * @param bandwidth Map containing the timestamps and the amount of data sent in the transmission
     * @param latency Latency measured
     */
    public Measure(String type, int ID, String sender,String receiver, Map<Long,Integer> bandwidth,double latency){
        this.type = type;
        this.ID = ID;
        this.sender = sender;
        this.receiver = receiver;
        this.bandwidth= bandwidth;
        this.latency = latency;
    }

    /**
     * Copy contructor
     * @param m Object that contain the measure
     */
    public Measure(Measure m){
        type = m.type;
        ID = m.ID;
        sender = m.sender;
        receiver = m.receiver;
        bandwidth= m.bandwidth;
        latency = m.latency;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public Map<Long, Integer> getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(Map<Long, Integer> bandwidth) {
        this.bandwidth = bandwidth;
    }

    public Double getLatency() {
        return latency;
    }

    public void setLatency(Double latency) {
        this.latency = latency;
    }
}

