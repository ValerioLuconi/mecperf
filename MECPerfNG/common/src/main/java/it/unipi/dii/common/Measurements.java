package it.unipi.dii.common;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Measurements {


    /**
     * Number of test made in the latency calculation
     */
    private static final int NUMLATENCYTEST = 100;


    /**
     * Size of the packet sent through the network
     */
    private static final int PKTSIZE_FOR_TCP_RTT = 1;
    private static final int PKTSIZE_FOR_UDP_LATENCY = 1;
    private static final long serialVersionUID = 3919700812200232178L;
    private static final int TCP_BANDWIDTH_BUFFER_LEN = 1024;
    public static final int TCPBANDWIDTH_NUM_OF_BYTES = 1024*TCP_BANDWIDTH_BUFFER_LEN; // 1MB


    /**
     * This function is used by the receiver to measure the RTT using the UDP protocol
     * The receiver receives a packet and sends another packet to the sender
     *
     * @param serverSocket The Socket use for the communication
     * @throws IOException
     */
    public static void UDPRTTReceiver(DatagramSocket serverSocket) throws IOException {
        byte[] sendData = new byte[PKTSIZE_FOR_UDP_LATENCY];
        byte[] receiveData = new byte[PKTSIZE_FOR_UDP_LATENCY];
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length);

        for (int i = 0; i < NUMLATENCYTEST; i++) {
            serverSocket.receive(receivePacket);
            InetAddress IPAddress = receivePacket.getAddress();
            sendPacket.setAddress(IPAddress);
            int port = receivePacket.getPort();
            sendPacket.setPort(port);
            serverSocket.send(sendPacket);
        }
    }

    /**
     * This function is used by the sender to connect with the receiver using the UDP protocol.
     * The sender starts the communication sending a packet and when receive a packet compute the latency.
     * After NUMLATENCYTEST communications, the function computes the mean latency value.
     *
     * @param connectionSocket The Socket use for the comunication
     * @return The mean latency value calculated using the Round Trip Time approach
     * @throws IOException
     */
    public static double UDPRTTSender(DatagramSocket connectionSocket) throws IOException {
        byte[] sendData = new byte[PKTSIZE_FOR_UDP_LATENCY];
        byte[] receiveData = new byte[PKTSIZE_FOR_UDP_LATENCY];
        Random rand = new Random();
        rand.nextBytes(sendData);

        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length);
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

        long meanValue = 0;
        for (int i = 0; i < NUMLATENCYTEST; i++) {
            long startTime = System.currentTimeMillis();
            connectionSocket.send(sendPacket);
            connectionSocket.receive(receivePacket);
            long endTime = System.currentTimeMillis();
            meanValue += endTime - startTime;
        }

        double result = ((double) meanValue) / NUMLATENCYTEST;
        return result;
    }

    /**
     * This function is used by the sender to connect with the receiver using the TCP protocol.
     * The sender starts the communication sending a packet and when receive a packet compute the latency.
     * After NUMLATENCYTEST communications, the function computes the mean latency value.
     *
     * @param socket The Socket use for the comunication
     * @return The mean latency calculated using the Round Trip Time
     * @throws IOException
     */
    public static double TCPRTTSender(Socket socket) throws IOException {
        OutputStream outputStream = null;
        InputStream inputStream = null;

        byte[] sendData = new byte[PKTSIZE_FOR_TCP_RTT];
        Random rand = new Random();
        rand.nextBytes(sendData);

        long total = 0;

        try {
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            long lBegin,
                 lEnd;

            for (int i = 0; i < NUMLATENCYTEST; i++) {
                outputStream.write(sendData, 0, PKTSIZE_FOR_TCP_RTT);
                lBegin = System.currentTimeMillis();
                inputStream.read();
                lEnd = System.currentTimeMillis();
                total += lEnd - lBegin;
            }
        } finally {
            if (inputStream != null)
                inputStream.close();
            if (outputStream != null)
                outputStream.close();
            if (socket != null)
                socket.close();
        }
        return ((double) (total)) / NUMLATENCYTEST; //non ho capito perchè fosse total/2
    }


    /**
     * This function is used by the receiver to measure the RTT using the UDP protocol
     * The receiver receives a packet and sends another packet to the sender
     *
     * @param sock The Socket use for the comunication
     */
    public static void TCPRTTReceiver(Socket sock) throws IOException {
        InputStream inputStream = null;
        OutputStream outputStream = null;

        byte[] sendData = new byte[PKTSIZE_FOR_TCP_RTT];
        Random rand = new Random();
        rand.nextBytes(sendData);

        try {
            inputStream = sock.getInputStream();
            outputStream = sock.getOutputStream();
            for (int i = 0; i < NUMLATENCYTEST; i++) {
                inputStream.read();
                outputStream.write(sendData, 0, PKTSIZE_FOR_TCP_RTT);
            }
        }finally {
            if (inputStream != null)
                inputStream.close();
            if (outputStream != null)
                outputStream.close();
            if (sock != null)
                sock.close();
        }
    }



    /**
     * This function is used by the sender to connect with the receiver using the TCP protocol.
     * The sender send a file through the network
     *
     * @param socket The Socket use for the comunication
     * @param number_of_bytes the number of bytes to be transferred
     */
    public static void TCPBandwidthSender(Socket socket, int number_of_bytes) throws IOException {
        OutputStream outputStream = null;
        byte[] buffer = new byte[TCP_BANDWIDTH_BUFFER_LEN];
        Random random = new Random();
        random.nextBytes(buffer);//fill the buffer with random bytes

        try {
            outputStream = socket.getOutputStream();

            while (number_of_bytes > 0) {
                if (number_of_bytes < TCP_BANDWIDTH_BUFFER_LEN) {
                    outputStream.write(buffer, 0, number_of_bytes);
                    number_of_bytes = 0;
                }
                else {
                    outputStream.write(buffer, 0, TCP_BANDWIDTH_BUFFER_LEN);
                    number_of_bytes -= TCP_BANDWIDTH_BUFFER_LEN;
                }
            }
        } finally {
            if(outputStream != null)
                outputStream.close();
            if(socket != null)
                socket.close();
        }
    }



    /**
     * This function is used by the receiver to connect with the sender using the TCP protocol.
     * The receiver receive the file and for each packet save in the map the timestamp and the amount of
     * data received.
     *
     * @param connectionSocket The Socket use for the comunication
     * @return The map that contain all the timestamps and the amount of data for each packet received
     */
    public static Map<Long, Integer> TCPBandwidthReceiver(Socket connectionSocket) throws IOException {
        InputStream isr = null;
        Map<Long, Integer> mappa = new HashMap<>();
        int totalRead;
        byte[] cbuf = new byte[TCP_BANDWIDTH_BUFFER_LEN];

        try {
            isr = connectionSocket.getInputStream();
            while ((totalRead = isr.read(cbuf)) != -1) {
                // misuro l'istante di tempo ogni volta che faccio la write
                mappa.put(System.nanoTime(), totalRead);
            }
        }
        finally{
            if (isr != null)
                isr.close();
            if (connectionSocket != null)
                connectionSocket.close();
        }

        return mappa;
    }



    /**
     * This function is used by the sender to connect with the receiver using the UDP protocol.
     * This function send two datagram packets through the network
     *
     */
    public static void UDPCapacityPPSender(DatagramSocket connectionSocket, int packet_size) {
        byte[] buf = new byte[packet_size];
        Random rand = new Random();
        rand.nextBytes(buf);

        DatagramPacket packet1 = new DatagramPacket(buf, buf.length);
        DatagramPacket packet2 = new DatagramPacket(buf, buf.length);

        //send 2 packets
        try{
            connectionSocket.send(packet1);
            connectionSocket.send(packet2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * This function is used by the receiver to connect with the sender using the UDP protocol.
     * This function receive two datagram packets and compute the difference between the two timestamps
     *
     * @param serverSocket The Socket use for the comunication
     * @param pktSize    Amount of data to receive for each comunication
     * @return The latency calculated using the packet pair approach
     */
    public static double UDPCapacityPPReceiver(DatagramSocket serverSocket, int pktSize) {
        byte[] receiveData1 = new byte[pktSize];
        byte[] receiveData2 = new byte[pktSize];

        //wait for first packet on socket
        DatagramPacket receivePacket1 = new DatagramPacket(receiveData1, receiveData1.length);
        DatagramPacket receivePacket2 = new DatagramPacket(receiveData2, receiveData2.length);

        try {
            serverSocket.receive(receivePacket1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        long firstTime = System.currentTimeMillis();
        //wait for second packet
        try {
            serverSocket.receive(receivePacket2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        double timems = System.currentTimeMillis() - firstTime;
        if (timems > 0)
            return (pktSize/timems);
        else
            return -1;
    }
}
