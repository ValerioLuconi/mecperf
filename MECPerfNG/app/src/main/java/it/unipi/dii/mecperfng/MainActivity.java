package it.unipi.dii.mecperfng;



import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import com.google.android.material.navigation.NavigationView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Map;
import it.unipi.dii.common.Measure;
import it.unipi.dii.common.Measurements;



public class MainActivity extends AppCompatActivity
                          implements NavigationView.OnNavigationItemSelectedListener {
    private static final int REQUEST_INTERNET = 1;
    private static String[] PERMISSIONS_INTERNET = {
                                                       Manifest.permission.INTERNET
                                                   };

    private static final int CMDPORT = 6792;//6789
    private static final int TCPPORT = 6791;//6788
    private static final int UDPPORT = 6790;//6787
    private static final int AGGRPORT = 6766;
    private static String OBSERVERIP = "";
    private static final int UDP_BANDWIDTH_PKTSIZE = 1024;
    private Integer id = 0;
    private AsyncCMD mAsyncTask = null;
    private SharedPreferences sp;



    private class Result {
        private boolean ok;
        private double value;
        private String type;



        public Result(double v, String t) {
            this(v, t, true);
        }



        public Result(double v, String t, boolean o) {
            value = v;
            type = t;
            ok = o;
        }



        public boolean isOK() {
            return ok;
        }
    }



    protected void sendDataToAggregator(String type, int id, String sender, String receiver,
                                        double latency, Map<Long, Integer> bandwidth) {
        Socket socket = null;
        ObjectOutputStream objOutputStream = null;
        String aggregatorIP = sp.getString("aggregator_address", "NA");

        try {
            socket = new Socket(InetAddress.getByName(aggregatorIP), AGGRPORT);
            objOutputStream = new ObjectOutputStream(socket.getOutputStream());
            Measure measure = new Measure(type, id, sender, receiver, bandwidth, latency);

            objOutputStream.writeObject(measure);
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        } finally {
            try {
                if (objOutputStream != null)
                    objOutputStream.close(); // close the output stream when we're done.
                if (socket != null)
                    socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    private class AsyncCMD extends AsyncTask<Void, Void, Result> {

        private String cmd;



        public AsyncCMD(String cmd) {
            super();
            this.cmd = cmd;
        }



        @Override
        protected Result doInBackground(Void... voids) {
            OBSERVERIP = sp.getString("observer_address", "");

            Spinner direction_spinner = findViewById(R.id.direction_spinner);
            String direction = direction_spinner.getItemAtPosition(direction_spinner
                                                .getSelectedItemPosition()).toString();
            try {
                switch (cmd) {
                    case "TCPBANDWIDTHBUTTON":
                        Socket communicationSocket = new Socket(InetAddress.getByName(OBSERVERIP),
                                                                                      TCPPORT);
                        if (direction.equals("Sender")) {
                            sendCMD("TCPBandwidthSender" + " " + id.toString());
                            id++;

                            Measurements.TCPBandwidthSender(communicationSocket,
                                                            Measurements.TCPBANDWIDTH_NUM_OF_BYTES);

                            return new Result(0.0, " TCP bandwidth sender completed");
                        } else {
                            sendCMD("TCPBandwidthReceiver" + " " + id.toString());

                            Map<Long, Integer> longIntegerMap = Measurements.
                                                          TCPBandwidthReceiver(communicationSocket);
                            sendDataToAggregator("TCPBandwidth", id, "Observer",
                                              "Client", -1, longIntegerMap);
                            id++;
                            return new Result(0.0, "TCP bandwidth receiver completed");
                        }

                    case "UDPBANDWIDTHBUTTON":
                        DatagramSocket connectionSocket = new DatagramSocket();
                        connectionSocket.connect(InetAddress.getByName(OBSERVERIP), UDPPORT);

                        if (direction.equals("Sender")) {
                            sendCMD("UDPCapacityPPSender" + " " + id.toString());
                            id++;

                            Measurements.UDPCapacityPPSender(connectionSocket, UDP_BANDWIDTH_PKTSIZE);

                            return new Result(0.0, " UDP Bandwidth sender completed");
                        } else {
                            sendCMD("UDPCapacityPPReceiver" + " " + id.toString());

                            //Client has to send a packet to server to let the server knows Client's
                            // IP and Port
                            String outString = "Dummy message";
                            byte[] buf = outString.getBytes();
                            connectionSocket.send( new DatagramPacket(buf, buf.length));
                            double latency = Measurements.UDPCapacityPPReceiver(connectionSocket,
                                                                            UDP_BANDWIDTH_PKTSIZE);

                            sendDataToAggregator("UDP", id, "Observer", "Client",
                                                       latency, null);
                            id++;
                            return new Result(latency, " Byte/ms");
                        }

                    case "TCPRTTBUTTON":
                        communicationSocket = new Socket(InetAddress.getByName(OBSERVERIP), TCPPORT);

                        if (direction.equals("Sender")){
                            // the client application starts a TCP RTT measure (as sender) with
                            // the observer
                            sendCMD("TCPRTTC" + " " + id.toString());

                            double latency = Measurements.TCPRTTSender(communicationSocket);
                            sendDataToAggregator("TCPRTT", id, "Client",
                                               "Observer", latency, null);
                            id++;

                            return new Result(latency, "ms");
                        } else {
                            //the observer starts a TCP RTT measure using the mobile application
                            // as receiver
                            sendCMD("TCPRTTMO" + " " + id.toString());
                            id++;

                            Measurements.TCPRTTReceiver(communicationSocket);
                            return new Result(0.0, "Server TCP Latency Test");
                        }
                    case "UDPRTTBUTTON":
                        DatagramSocket udpsocket = new DatagramSocket();
                        udpsocket.connect(InetAddress.getByName(OBSERVERIP), UDPPORT);

                        if (direction.equals("Sender")) {
                            // the client application starts a TCP RTT measure as sender. The
                            // observer is the receiver

                            sendCMD("UDPRTTC" + " " + id.toString());
                            double latency = Measurements.UDPRTTSender(udpsocket);
                            sendDataToAggregator("UDP", id, "Client",
                                              "Observer", latency, null);
                            id++;
                            return new Result(latency, "Ms");
                        } else {
                            // the client application starts a TCP RTT measure with the observer as
                            //sender

                            sendCMD("UDPRTTMO" + " " + id.toString());
                            id++;
                            String outString = "DummyPacket";
                            byte[] buf = outString.getBytes();
                            udpsocket.send(new DatagramPacket(buf, buf.length));
                            Measurements.UDPRTTReceiver(udpsocket);
                            return new Result(0.0, "Server UDP Latency Test");
                        }
                }
            } catch (IOException ioe) {
                return new Result(0, ioe.getMessage(), false);
            }
            return null;
        }



        @Override
        protected void onPostExecute(Result result) {
            TextView resultTextView = findViewById(R.id.resultTextView);

            if(result != null)
                if (result.isOK()) {
                    resultTextView.setText(getString(R.string.results_ValuesType,
                            Double.toString(result.value), result.type));
                } else {
                    resultTextView.setText(getString(R.string.results_Error, result.type));
                }
        }



        @Override
        protected void onCancelled() {
            mAsyncTask = null;
        }



        private void sendCMD(String command) throws IOException {
            OBSERVERIP = sp.getString("observer_address", "");

            Socket socket = new Socket(InetAddress.getByName(OBSERVERIP), CMDPORT);

            // get the output stream from the socket.
            OutputStream outputStream = socket.getOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);

            // write the message we want to send
            dataOutputStream.writeUTF(command);
            dataOutputStream.flush(); // send the message
            dataOutputStream.close(); // close the output stream when we're done.
        }
    }



    /**
     * Checks if the app has permission to write to device storage
     * <p>
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity .
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.INTERNET);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
                ActivityCompat.requestPermissions(activity, PERMISSIONS_INTERNET, REQUEST_INTERNET);

        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        Spinner function_spinner = findViewById(R.id.direction_spinner);
        CharSequence[] fun = getResources().getStringArray((R.array.function_array));
        ArrayAdapter<CharSequence> adfun = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, fun);
        adfun.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        function_spinner.setAdapter(adfun);

        verifyStoragePermissions(this);

        Button TCPButton = findViewById(R.id.TCPBandwidthbutton);
        TCPButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAsyncTask = new AsyncCMD("TCPBANDWIDTHBUTTON");
                mAsyncTask.execute((Void) null);
                Log.d("TASK", "Task Eseguito");
            }
        });

        Button UDPButton = findViewById(R.id.UDPBandwidthbutton);
        UDPButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAsyncTask = new AsyncCMD("UDPBANDWIDTHBUTTON");
                mAsyncTask.execute((Void) null);
                Log.d("TASK", "Task Eseguito");
            }
        });

        Button TCPRTTButton = findViewById(R.id.TCPRTTbutton);
        TCPRTTButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAsyncTask = new AsyncCMD("TCPRTTBUTTON");
                mAsyncTask.execute((Void) null);
            }
        });

        Button UDPRTTButton = findViewById(R.id.UDPRTTbutton);
        UDPRTTButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAsyncTask = new AsyncCMD("UDPRTTBUTTON");
                mAsyncTask.execute((Void) null);
            }
        });
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_results) {
            startActivity(new Intent(this, ResultsActivity.class));
        } else if (id == R.id.nav_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
