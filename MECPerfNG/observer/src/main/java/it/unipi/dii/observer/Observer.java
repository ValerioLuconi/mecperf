package it.unipi.dii.observer;

/*
javac Measurements/src/measurements/Measurements.java Measure/src/measure/Measure.java Observer/src/observer/Observer.java
java -cp ".:Measure/src/:Measurements/src/:Observer/src/" observer.Observer
*/

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import it.unipi.dii.common.Measurements;
import it.unipi.dii.common.Measure;


public class Observer {

    private static final String SERVERIP = "131.114.73.3";
    private static final int CMDPORT = 6789;
    private static final int TCPPORT = 6788;
    private static final int UDPPORT = 6787;
    private static final String AGGREGATORIP = "131.114.73.3";
    private static final int AGGRPORT = 6766;
    private static final int OBSCMDPORT = 6792;
    private static final int OBSTCPPORT = 6791;
    private static final int OBSUDPPORT = 6790;
    private static final int PKTSIZE = 1024;


    public static void main(String[] args) throws NumberFormatException, Exception{
        ServerSocket cmdListener = null;
        ServerSocket tcpListener = null;
        DatagramSocket udpListener = null;

        try {
            cmdListener = new ServerSocket(OBSCMDPORT);//socket used to receive commands
            tcpListener = new ServerSocket(OBSTCPPORT);//socket used for tcp operations
            udpListener = new DatagramSocket(OBSUDPPORT);//socket used for udp operations
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Observer CMD: inizializzato sulla porta " + cmdListener.getLocalPort());
        System.out.println("Observer TCP: inizializzato sulla porta " + tcpListener.getLocalPort());
        System.out.println("Observer UDP: inizializzato sulla porta " + udpListener.getLocalPort());

        while (true) {

            Socket cmdSocket = null;
            try {
                cmdSocket = cmdListener.accept();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            // get the input stream from the connected socket
            InputStream inputStream = null;
            try {
                inputStream = cmdSocket.getInputStream();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            // create a DataInputStream so we can read data from it.
            DataInputStream dataInputStream = new DataInputStream(inputStream);

            // read the message from the socket
            String cmd = null;
            try {
                //The command received is composed by "command id-test"
                cmd = dataInputStream.readUTF();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            //command received is <command ; id>
            String separator =" ";
            String[] cmdSplitted = cmd.split(separator);

            System.out.println("\nThe cmd sent from the socket was: " + cmdSplitted[0]);

            double latency = 0.0;
            switch(cmdSplitted[0]){

                case "TCPBandwidthSender":
                    /*
                        the app sends packets to the observer
                        the observer sends packet to the server
                    */
                    try {
                        Socket tcpReceiverConnectionSocket = tcpListener.accept();
                        Socket tcpSenderConnectionSocket =  new Socket(InetAddress.getByName(SERVERIP), TCPPORT);

                        //receive from the application
                        Map<Long, Integer>  mappaCO = Measurements.TCPBandwidthReceiver(tcpReceiverConnectionSocket);
                        sendAggregator("TCPBandwidth", Integer.parseInt(cmdSplitted[1]),
                                     "Client", "Observer", -1, mappaCO);

                        //send to theremote server
                        sendCMD(cmd);
                        Measurements.TCPBandwidthSender(tcpSenderConnectionSocket,
                                                        Measurements.TCPBANDWIDTH_NUM_OF_BYTES);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    System.out.println("TCPBandwidth: command finished");
                    break;

                case "TCPBandwidthReceiver":
                    /*
                        the observer sends packets to the app
                        the remote server sends packet to the observer
                    */
                    Map<Long, Integer> mappaSO;

                    try {
                        Socket tcpSenderConnectionSocket = tcpListener.accept();
                        Socket tcpReceiverConnectionSocket = new Socket(InetAddress.getByName(SERVERIP), TCPPORT);

                        Measurements.TCPBandwidthSender(tcpSenderConnectionSocket, Measurements.TCPBANDWIDTH_NUM_OF_BYTES);

                        sendCMD(cmd);
                        mappaSO = Measurements.TCPBandwidthReceiver(tcpReceiverConnectionSocket);

                        sendAggregator("TCPBandwidth", Integer.parseInt(cmdSplitted[1]), "Server", "Observer", -1 , mappaSO);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    System.out.println("TCPBandwidth: command finished");
                    break;

                case "UDPCapacityPPSender":
                    //MO sends the metrics client-observer and forwards the command to the server
                    try {
                        latency = Measurements.UDPCapacityPPReceiver(udpListener,PKTSIZE);

                        //è una banda o una latenza?
                        //sendAggregator("UDP", Integer.parseInt(cmdSplitted[1]), "Client", "Observer", latency , null);
                        sendAggregator("UDP", Integer.parseInt(cmdSplitted[1]), "Client", "Observer", latency, null);
                        //obtained by packet-pair
                        System.out.println("Observer UDP Latency : " + latency + " Ns");

                        sendCMD(cmd);

                        DatagramSocket udpsocket = new DatagramSocket();
                        udpsocket.connect(InetAddress.getByName(SERVERIP),UDPPORT);
                        Measurements.UDPCapacityPPSender(udpsocket, PKTSIZE);
                    } catch (SocketException | UnknownHostException ex) {
                        //Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                        ex.printStackTrace();
                    }
                    System.out.println("Finished!");
                    break;

                case "UDPCapacityPPReceiver":
                    //MO forwards the command to the server and sends the metrics server-observer to the aggregator
                    //MO has to receive a packet from the client to know Client's Address and Port
                    byte[] buf = new byte[1000];
                    DatagramPacket dgp = new DatagramPacket(buf, buf.length);
                    try {
                        udpListener.receive(dgp);
                    } catch (IOException ex) {
                        Logger.getLogger(Observer.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    udpListener.connect(dgp.getAddress(), dgp.getPort());
                    Measurements.UDPCapacityPPSender(udpListener, PKTSIZE);
                    udpListener.disconnect();

                    sendCMD(cmd);

                    DatagramSocket udpsocket = null;
                    try {
                        udpsocket = new DatagramSocket();
                        byte[] buff;
                        String outString = "UDPCapacityPPReceiver";
                        buff = outString.getBytes();
                        DatagramPacket out = new DatagramPacket(buff, buff.length, InetAddress.getByName(SERVERIP), UDPPORT);
                        //MO has to send a packet to server to let the server knows Client's IP and Port
                        udpsocket.send(out);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    latency = Measurements.UDPCapacityPPReceiver(udpsocket,PKTSIZE);
                    // send data to Aggregator
                    sendAggregator("UDP", Integer.parseInt(cmdSplitted[1]), "Server", "Observer", latency , null);
                    break;

                case "UDPRTTC":
                    //MO forwards the command to the server and sends the metrics observer-server
                    try {
                        Measurements.UDPRTTReceiver(udpListener);

                        sendCMD("UDPRTTMO" + " " + cmdSplitted[1]);
                        DatagramSocket udpsocketmo = new DatagramSocket();
                        udpsocketmo.connect(InetAddress.getByName(SERVERIP),UDPPORT);
                        latency = Measurements.UDPRTTSender(udpsocketmo);

                        sendAggregator("UDP", Integer.parseInt(cmdSplitted[1]), "Observer", "Server", latency , null);

                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    break;

                case "UDPRTTMO":
                    try {
                        //MO has to receive a packet from the client to know Client's Address and Port

                        byte[] bufrtt = new byte[1000];
                        DatagramPacket dgprtt = new DatagramPacket(bufrtt, bufrtt.length);
                        try {
                            udpListener.receive(dgprtt);
                        } catch (IOException ex) {
                            Logger.getLogger(Observer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        udpListener.connect(dgprtt.getAddress(), dgprtt.getPort());


                        latency = Measurements.UDPRTTSender(udpListener);
                        sendAggregator("UDP", Integer.parseInt(cmdSplitted[1]), "Observer", "Client", latency , null);
                        udpListener.disconnect();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    System.out.println("MO UDP RTT : " + latency + " Ms");

                    sendCMD("UDPRTTMRS" + " " + cmdSplitted[1]);

                    try{
                        DatagramSocket udpsocketRTT = new DatagramSocket();
                        byte[] bufRTT;
                        String outString = "UDPCapacityPPReceiver";
                        bufRTT = outString.getBytes();
                        DatagramPacket out = new DatagramPacket(bufRTT, bufRTT.length, InetAddress.getByName(SERVERIP), UDPPORT);
                        //Client has to send a packet to server to let the server knows Client's IP and Port
                        udpsocketRTT.send(out);
                        Measurements.UDPRTTReceiver(udpsocketRTT);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    break;

                case "TCPRTTC":
                    //the client start a TCP RTT measure using the observer as receiver
                    //the observer starts a TCP RTT measure using the remote server as receiver
                    try {
                        //receive from the client
                        Socket tcpRTTClient = tcpListener.accept();
                        Measurements.TCPRTTReceiver(tcpRTTClient);

                        //send to the remote server
                        sendCMD("TCPRTTMO" + " " + cmdSplitted[1]);
                        latency = Measurements.TCPRTTSender(new Socket(InetAddress.getByName(SERVERIP), TCPPORT));
                        sendAggregator("TCPRTT", Integer.parseInt(cmdSplitted[1]), "Observer", "Server", latency , null);
                        System.out.println("Observer TCP RTT : " + latency + " Ms");
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    break;

                case "TCPRTTMO":
                    //MO sends metrics client-observer and forwards the command to the server
                    try {
                        Socket tcpRTT = tcpListener.accept();
                        latency = Measurements.TCPRTTSender(tcpRTT);
                        sendAggregator("TCPRTT", Integer.parseInt(cmdSplitted[1]), "Observer", "Client", latency , null);
                        System.out.println("MO TCP Latency : " + latency + " Ms");

                        sendCMD("TCPRTTMRS" + " " + cmdSplitted[1]);
                        Measurements.TCPRTTReceiver(new Socket(InetAddress.getByName(SERVERIP), TCPPORT));

                    } catch (IOException ex) {
                        //Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                        ex.printStackTrace();
                    }
                    break;
            }
        }
    }

    protected static void sendCMD(String command){
        Socket socket = null;
        try {
            socket = new Socket(InetAddress.getByName(SERVERIP), CMDPORT);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        // get the output stream from the socket.
        OutputStream outputStream = null;
        try {
            outputStream = socket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // create a data output stream from the output stream so we can send data through it
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
        try{
            // write the message we want to send
            dataOutputStream.writeUTF(command);
            dataOutputStream.flush(); // send the message
            dataOutputStream.close(); // close the output stream when we're done.
        }catch (IOException e){
        }
    }

    protected static void sendAggregator(String type, int id, String sender, String receiver, double latency, Map<Long, Integer> bandwidth)
            throws Exception, IOException {
        Socket socket = null;
        ObjectOutputStream objOutputStream = null;
        try {
            socket = new Socket(InetAddress.getByName(AGGREGATORIP), AGGRPORT);
            objOutputStream = new ObjectOutputStream(socket.getOutputStream());
            // public Measure(String type, int ID, String sender,String receiver, Map<Long,Integer> bandwidth,double latency){
            Measure measure = new Measure(type, id, sender, receiver, bandwidth, latency);

            // write the message we want to send
            objOutputStream.writeObject(measure);
        } finally {
            objOutputStream.close(); // close the output stream when we're done.
            socket.close();
        }
    }
}


