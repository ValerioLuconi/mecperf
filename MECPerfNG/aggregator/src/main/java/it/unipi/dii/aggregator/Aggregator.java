package it.unipi.dii.aggregator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import it.unipi.dii.common.Measure;



public class Aggregator {
    private static final String TCP_OUTPUT_FILE = "./Measurement/TCP.csv";
    private static final String UDP_OUTPUT_FILE = "./Measurement/UDP.csv";
    private static final String MAP_OUTPUT_FILE = "./Measurement/Maps.csv";
    private static final String CSV_SEPARATOR = ";";

    public static void main (String[] args){
        File TCPfile = null;
        File UDPfile = null;
        ServerSocket welcomeSoket = null;

        try {
            if (Files.notExists(Paths.get(TCP_OUTPUT_FILE)))
                TCPfile.createNewFile();

            if (Files.notExists(Paths.get(UDP_OUTPUT_FILE)))
                UDPfile.createNewFile();

            TCPfile = new File(TCP_OUTPUT_FILE);
            UDPfile = new File(UDP_OUTPUT_FILE);


            welcomeSoket = new ServerSocket(6766);
        }
        catch (IOException | NullPointerException e ){
            e.printStackTrace();
        }


        while (true) {
            try {
                Socket connectionSocket = welcomeSoket.accept();

                InputStream isr = connectionSocket.getInputStream();
                ObjectInputStream mapInputStream = new ObjectInputStream(isr);
                Measure measure = (Measure) mapInputStream.readObject();

                switch(measure.getType()){
                    case "TCPBandwidth": case "TCPRTT":
                        writeToCSVTCP(measure);
                        break;

                    case "UDP":
                        writeToCSVUDP(measure);
                        break;
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                break;
            }
        }
        try {
            if (welcomeSoket != null)
                welcomeSoket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Write the received measure and write it into the TCP file
     * @param type the type of the measure
     * @param m received measuere
     */

    private static void writeToCSVTCP(Measure m) {
        try
        {
            BufferedWriter bw = new BufferedWriter(new FileWriter(TCP_OUTPUT_FILE, true));
            StringBuffer oneLine = new StringBuffer();
            oneLine.append(m.getID());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(m.getSender());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(m.getReceiver());
            oneLine.append(CSV_SEPARATOR);

            System.out.println(m.getType() +": " + m.getSender() + " -> " + m.getReceiver());

            if (m.getType().compareTo("TCPBandwidth") == 0)
            {
                oneLine.append("Data in File");

                BufferedWriter bwM = new BufferedWriter(new FileWriter(MAP_OUTPUT_FILE, true));
                StringBuffer oneLineM = new StringBuffer();

                for (Map.Entry<Long, Integer> entry : m.getBandwidth().entrySet()) {
                    oneLineM.append(m.getID());
                    oneLineM.append(CSV_SEPARATOR);
                    oneLineM.append(entry.getKey());
                    oneLineM.append(CSV_SEPARATOR);
                    oneLineM.append(entry.getValue());
                    oneLineM.append(CSV_SEPARATOR);
                    oneLineM.append(m.getSender());
                    oneLineM.append(CSV_SEPARATOR);
                    oneLineM.append(m.getReceiver());
                    oneLineM.append(CSV_SEPARATOR);
                }

                bwM.write(oneLineM.toString());
                bwM.newLine();
                bwM.flush();
                bwM.close();
            }
            else
                oneLine.append(m.getLatency());


            bw.write(oneLine.toString());
            bw.newLine();
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Write the received measure and write it into the UDP file
     * @param m received measuere
     */

    private static void writeToCSVUDP(Measure m)
    {
        System.out.println(m.getType() +": " + m.getSender() + " -> " + m.getReceiver());

        try
        {
            BufferedWriter bw = new BufferedWriter(new FileWriter(UDP_OUTPUT_FILE, true) );

            StringBuffer oneLine = new StringBuffer();
            oneLine.append(m.getID());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(m.getSender());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(m.getReceiver());
            oneLine.append(CSV_SEPARATOR);
            oneLine.append(m.getLatency());
            bw.write(oneLine.toString());
            bw.newLine();

            bw.flush();
            bw.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}

