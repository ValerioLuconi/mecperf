Per utilizzare tale progetto è necessario che Aggregator e Server siano eseguiti su una macchina dove non sia presente il NAT o sulla stessa rete dei processi Observer e MecPerfClientT.

Copiare i file Server.jar e Aggregator.jar in una directory della macchina suddetta e nella stessa directory devono essere presenti le cartelle sources/100Mb.dat e Measurement/ (contenente i file Maps.csv, TCP.csv, UDP.csv)

Le statistiche relative alle misurazioni verrano salvate nei file TCP.csv e UDP.csv mentre in Maps.csv vengono salvate map <timestamp, bytes> relative a pacchetti tcp inviati, tali dati saranno poi utilizzati per calcolare la banda.

Modificare adeguatamente le variabili SERVERIP ed AGGREGATORIP nel MainActivity.java del client mobile ed in Observer.java 

Assicurarsi di copiare o spostare il file 100Mb.dat nella cartella Sdcard/Downloads del dispositivo mobile.

Si avviano quindi Server.jar, Aggregator.jar, Observer.jar(sulla stessa rete di MecPerfClientT) e MecperfClientT.

L'utente interagisce mediante l'interfaccia offerta da MecperfClientT da un dispositivo mobile. Mediante un menù a tendina può scegliere se effettuare il test per il download o l'upload (Receiver/Sender) e mendiante bottoni può selezionare il test desiderato.

